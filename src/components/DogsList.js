import React, { useState } from 'react'

const Dogs = [
    {
        "title": "Siberian Husky",
        "image": "Images/husky.jpg",
        "description": "The Siberian Husky dog breed has a beautiful, thick coat that comes in a multitude of colors and markings. Their blue or multi-colored eyes and striking facial masks only add to the appeal of this breed, which originated in Siberia."
    },
    {
        "title": "Chow Chow",
        "image": "Images/chow.jpg",
        "description": "The distinctive-looking Chow Chow dog breed has a proud, independent spirit that some describe as catlike. They can be aloof — if you’re looking for a cuddle buddy, this probably isn’t the best breed for you — and downright suspicious of strangers. But for the right person, they can be a fiercely loyal companion."
    },
    {
        "title": "German Shepherd",
        "image": "Images/german.jpg",
        "description": "The German Shepherd Dog is one of America’s most popular dog breeds — for good reasons. They’re intelligent and capable working dogs. Their devotion and courage are unmatched. And they’re amazingly versatile."
    },
    {
        "title": "Golden Retrievers",
        "image": "Images/golden.jpg",
        "description": "Golden Retrievers are loved by people because of their caring, friendly and loyal nature. They are great family dogs and make for an ideal companion for many people. Some people think that Golden Retrievers only come in gold color. This isn’t true. Golden Retrievers are also available in red color. These dogs are referred to as Red Golden Retrievers."
    },
    {
        "title": "Bulldog",
        "image": "Images/boolDog.jpg",
        "description": "The Bulldog was originally used to drive cattle to market and to compete in a bloody sport called bullbaiting. Today, they’re gentle companions who love kids."
    },
    {
        "title": "Pomeranian",
        "image": "Images/pomarian.jpg",
        "description": "Descended from large sled dog breeds, the now-tiny Pomeranian has a long and interesting history. The foxy-faced dog, nicknamed “the little dog who thinks he can,” is compact, active, and capable of competing in agility and obedience or simply being a family friend."
    }

]
function DogsList() {
    const [dogData, setDogData] = useState(Dogs);

    return (
        <>
            <div className="md:w-3/5 sm:1/2 m-auto font-serif ">
                <h1 className="mt-16 font-bold md:text-3xl sm:text-2xl">Information about Dogs</h1>
                <div className="p-16 grid grid-cols-1 sm:grid-cols-1 md:grid-cols-1 lg:grid-cols-1 xl:grid-cols-1  gap-5 mt-3 ">
                    {
                        dogData.map((item, i) => (
                            <div className="rounded overflow-hidden shadow bg-gray-50" id={i}>
                                <div className="px-6 pt-4 pb-2">
                                    <h2 className="font-bold pb-3 pt-5 md:text-3xl sm:text-xl">{item.title}</h2>
                                    <div className="md:flex-none lg:flex sm:flex xl:flex m-auto p-5">
                                        <img src={item.image} alt={item.title} className="m-auto pb-2 md:w-40 md:h-40 md:rounded-full sm:w-24  sm:h-24 sm:rounded-full  " />
                                        <p className="md:pl-16 pt-5 md:text-justify sm:text-center sm:pl-0 ">{item.description}</p>
                                    </div>
                                </div>
                            </div>
                        ))
                    }
                </div>
            </div>
        </>
    )
}
export default DogsList
