import React, { useState } from 'react'
const Contact_Us = [
    {
        "email": "sona123@gmail.com",
        "description": "For any question and support",
        "address": "Dogs Shelter, Near Janki Tokies, Nagpur -444444",
        "formContent": "Let us know your questions, suggestions and concerns by filling out the contact form below."
    }
]
const Contact = () => {
    const initialState = {
        email: "",
        password: "",
        errors: {}
    }
    const [contact, setContact] = useState(Contact_Us);
    const [emailData, setEmail] = useState(initialState);

    const onChange = (event) => {
        const value = event.target.value;
        console.log(value)
        const name = event.target.name;
        setEmail({ ...emailData, [name]: value })
    }
    const onSubmit = (event) => {
        event.preventDefault();
        alert("Login successfully...")
    }
    return (
        <div className="md:w-3/5 sm:1/2 shadow bg-gray-200 m-auto p-20 mb-20 mt-20">
            <div className="">
                {
                    contact.map((item) => (
                        <>
                            <p className="pb-2 font-semibold bg-black-500 mb-10">{item.formContent}</p>
                            <div className="md:flex sm:flex-none ">
                                <div className="p-10   pb-10">
                                    <p className="pb-2  font-semibold bg-black-500">{item.description} :</p>
                                    <p className="pb-2  font-semibold bg-black-500">Email at :<a href="#" className="text-blue-8font-bold"> {item.email}</a></p>
                                    <p className="pb-2  font-semibold bg-black-500">Address : {item.address}</p>
                                </div>
                                <div className="w-full max-w-sm border shadow ml-50 bg-gray-50 pb-10">
                                    <h3 className="font-bold pt-5 text-xl">Contact Us</h3>
                                    <form onSubmit={onSubmit}>
                                        <div className="m-5 pl-0  ">
                                            <input className="bg-gray-200 appearance-none border-2 border-gray-200
                                                        rounded w-full py-2 px-4 text-gray-700 leading-tight
                                                        focus:outline-none focus:bg-white focus:border-purple-500"
                                                required
                                                type="email"
                                                name="email"
                                                placeholder="Enter an email"
                                                value={emailData.email}
                                                onChange={onChange}
                                            />
                                        </div>
                                        <div className="m-5 pl-0">
                                            <input className="bg-gray-200 appearance-none border-2 border-gray-200
                                                        rounded w-full py-2 px-4 text-gray-700 leading-tight
                                                        focus:outline-none focus:bg-white focus:border-purple-500"
                                                required
                                                type="password"
                                                name="password"
                                                placeholder="Enter a password"
                                                value={emailData.password}
                                                onChange={onChange}
                                            />
                                        </div>
                                        <button className="bg-purple-600 hover:bg-purple-800 text-white md:font-bold 
                                                        sm:font-sm py-2 px-4 rounded-full md:mt-3 sm:mt-3"
                                            type="submit">
                                            Submit
                                      </button>
                                    </form>
                                </div>
                            </div>
                        </>
                    ))
                }
            </div>
        </div>
    )
}
export default Contact
