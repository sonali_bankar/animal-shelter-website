import React from 'react'
import About from './About'
import AnimalSlider from './AnimalSlider'
const Home = () => {
    return (
        <div>
            <About />
            <AnimalSlider />
        </div>
    )
}
export default Home
