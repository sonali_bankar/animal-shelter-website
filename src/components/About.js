import React, { useState } from 'react'

const Facts = [
    {
        "fact": "Dogs have a sense of time. It's been proven that they know the difference between one hour and five."
    },
    {
        "fact": "Your dog is as smart as a two-year old! Ever wonder why children around this age seem to have a special bond with the family dog."
    },
    {
        "fact": "A study at UCSD claims that your dog can genuinely get jealous when they see you display affection for another creature."
    },
    {
        "fact": "Stray dogs in Russia have learned how to ride the complex subway system, and get off at specific stops in search of food."
    },
    {
        "fact": "Your dog can smell your feelings. In fact, your dog’s sense of smell is approximately 100,000 times better than yours. So it shouldn’t be shocking that they can in fact, smell things such as fear."
    }
]

const About = () => {
    const [facts, setFacts] = useState(Facts);
    return (
        <div>
            <h1 className="mt-16 font-bold md:text-3xl sm:text-2xl pb-3">Amazing Fact About Dogs</h1>
            <div className="md:w-2/3 sm:1/2 m-auto font-serif border border-black-200 mt-10 p-10 shadow bg-gray-50">

                <div className="md:flex sm:flex-none">
                    <img src="Images/chow.jpg" className="md:w-1/2 md:h-1/2 sm:w-full sm:h-full pt-6" />

                    <ol>
                        {
                            facts.map((item) => (
                                <li className="pl-5 pb-5 font-600 text-xl sm:mt-5">{item.fact}</li>
                            ))
                        }
                    </ol>
                </div>
            </div>
        </div>
    )
}
export default About
