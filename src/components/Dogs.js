import React from 'react'
import DogsList from './DogsList'

const Dogs = () => {
    return (
        <div>
            <DogsList/>
        </div>
    )
}

export default Dogs
