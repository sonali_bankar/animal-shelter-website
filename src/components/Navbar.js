import React from 'react'
import { Link } from "react-router-dom";

const Navbar = () => {
    return (
        <header className="bg-gray-800 text-white p-5 ">
            <a href="#" className=" pt-3 pl-5 flex">
                <img className=" rounded-full h-12 w-12" src="Images/logo.png" alt="Logo"/>
                <h4 className="pl-5 pt-3 font-bold">Dogs Shelter</h4>
            </a>
            <nav className="mb-5 mt-auto text-md  uppercase font-800">
                <Link to="/" className="p-6 pb-3 hover:underline ">Home</Link>
                <Link to="/dogs" className="p-6 hover:underline">Dogs</Link>
                <Link to="/contact" className="p-6 hover:underline">Contact</Link>
            </nav>
        </header>
    )
}
export default Navbar


