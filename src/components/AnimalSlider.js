import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import Slider from "react-slick";

// Import css files
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const Dogs = [
    {
        "title": "Siberian Husky",
        "image": "Images/husky.jpg",
        "description": "The Siberian Husky dog breed has a beautiful, thick coat that comes in a multitude of colors and markings. Their blue or multi-colored eyes and striking facial masks only add to the appeal of this breed."
    },

    {
        "title": "German Shepherd",
        "image": "Images/german.jpg",
        "description": "The German Shepherd Dog is one of America’s most popular dog breeds — for good reasons. They’re intelligent and capable working dogs. Their devotion and courage are unmatched. And they’re amazingly versatile."
    },
    {
        "title": "Golden Retrievers",
        "image": "Images/golden.jpg",
        "description": "Golden Retrievers are loved by people because of their caring, friendly and loyal nature.  Some people think that Golden Retrievers only come in gold color. Golden Retrievers are also available in red color."
    },


]


function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div
            className={className}
            style={{ ...style, display: "block", background: "black", color: "#ffffff" }}
            onClick={onClick}
        />
    );
}

function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div
            className={className}
            style={{ ...style, display: "block", background: "black", color: "#ffffff" }}
            onClick={onClick}
        />
    );
}


const AnimalSlider = () => {
    const config = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 2,
        slidesToScroll: 1,
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />
    };
    const [settings, setSettings] = useState(config);
    const [animal, setAnimalData] = useState(Dogs);
    const history = useHistory();
    const onClick = () => {
        history.push("/dogs")
    }
    return (
        <div class=" md:w-2/3 sm:w-1/2  h-auto  pl-0 mt-6 m-auto mb-10">
            <h1 className="mt-16 font-bold md:text-3xl sm:text-2xl mb-5 ">List of Dogs</h1>
            <Slider {...settings}>
                {
                    animal.map((item, i) => (
                        <div class="rounded " id={i} >
                            <div class="p-2 m-5 border shadow bg-gray-50">
                                <h2 className="font-bold pb-3 pt-5  md:text-xl sm:text-lg">{item.title}</h2>
                                <img src={item.image} alt={item.title} className="m-auto pb-2 w-36 h-36 rounded-full " />
                                <p className="  md:text-center sm:text-center  ">{item.description.slice(0, 100)}</p>
                            </div>
                        </div>
                    ))
                }
            </Slider>
            <button class="bg-purple-600 hover:bg-purple-800 text-white md:font-bold
             sm:font-sm py-2 px-4 rounded-full md:mt-6 sm:mt-4">
                <Link onClick={onClick}>View All</Link>
            </button>
        </div>
    );
}
export default AnimalSlider
