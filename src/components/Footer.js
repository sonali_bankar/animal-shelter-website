import React from 'react'
import *  as TiIcon from 'react-icons/ti';
import *  as SiIcon from 'react-icons/si';
import { Link } from 'react-router-dom';
function Footer() {
    return (
        <footer className="bg-gray-800 p-10 ">
            <div className="flex ">
                <p className="text-center m-auto text-white font-300 font-serif">All Rights Reserved</p>
                <a href="https://www.facebook.com/"><TiIcon.TiSocialFacebookCircular className="w-16 h-16 bg-white-100 pr-5 " /></a>
                <a href="https://twitter.com/login?lang=en"><TiIcon.TiSocialTwitter className="w-16 h-16 bg-white-100 pr-5 " /></a>
                <a href="https://www.instagram.com/accounts/login/"><SiIcon.SiInstagram className="w-14 h-14 bg-white-100 pr-5 " /></a>
                </div>
            <p className="text-white font-300 font-serif text-left">Copyright &copy; Dogs Shelter by Sonali Bankar</p>
        </footer>
    )
}

export default Footer
