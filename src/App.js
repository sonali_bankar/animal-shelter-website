import './App.css';
import Navbar from './components/Navbar';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from './components/Home';
import Footer from './components/Footer';
import Dogs from './components/Dogs';
import Contact from './components/Contact';

function App() {
  return (
    <div className="App bg-indigo-50 ">
      <Router>
        <Navbar />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/dogs" component={Dogs} />
          <Route path="/contact" component={Contact} />
        </Switch>
        <Footer />
      </Router>
    </div>
  );
}
export default App;
